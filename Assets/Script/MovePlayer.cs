using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private Joystick touch;
   //SerializeField] private FixedJoystick touchSkill;
    [SerializeField] Animator anmPlayer;

    
    
   
    private void Start()
    {
        anmPlayer = GetComponent<Animator>();

        anmPlayer.ResetTrigger("Move");
        anmPlayer.SetTrigger("Idle");
       // test
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        float xMove = touch.Horizontal;
        float zMove = touch.Vertical;

        Vector3 direction = new Vector3(xMove, 0, zMove);
        Vector3 lookAt = transform.position + direction;

        transform.position += new Vector3(xMove, 0f, zMove) * speed * Time.deltaTime;
        anmPlayer.ResetTrigger("Idle");
        anmPlayer.SetTrigger("Move");

        transform.LookAt(lookAt);
    
    //gameObject.transform.rotation = Quaternion.LookRotation(transform.position, touch.Direction);

    }

   
}
