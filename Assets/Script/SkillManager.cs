using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SkillManager : MonoBehaviour
{
    [Header("Ability 1")]
    [SerializeField] Image ability1;
    [SerializeField] float coolDown = 5;
    [SerializeField] GameObject skill1;
    [SerializeField] GameObject touch1;
    [SerializeField] Transform player1;

    [SerializeField] FixedJoystick rotateSkill;
    bool isCoolDown = false;


    [Header("Ability 2")]
    [SerializeField] Image ability2;
    [SerializeField] float coolDown2 = 10;
    bool isCoolDown2 = false;

    private void Start()
    {
        ability1.fillAmount = 0;
        ability2.fillAmount = 0;


        skill1.SetActive(false);
        touch1.SetActive(false);

    }

    bool check1 = false;
    bool check2 = false;
    bool check3 = false;

    private void Update()
    {
        if(check1 == true )
        {
            Ability1();
        }
        if (check2 == true )
        {
            Ability2();
        }
        float xMove = rotateSkill.Horizontal;
        float zMove = rotateSkill.Vertical;

        Vector3 direction = new Vector3(xMove, 0, zMove);
        Vector3 lookAt = transform.position + direction;
        Quaternion transRot = Quaternion.LookRotation(direction - player1.transform.position);
      //  skill1.transform.rotation = ; 
        //transform.LookAt(lookAt);


    }
    public void Ability1()
    {
        check1 = true;
        if (Input.touchCount > 0 && isCoolDown == false  )
        {

            isCoolDown = true;
            ability1.fillAmount = 1;
            skill1.SetActive(true);
            touch1.SetActive(true);

           // Vector3 dir = 
         


        }
        
   
        if (isCoolDown)
        {
            
            ability1.fillAmount -= 1 / coolDown * Time.deltaTime;
            if (ability1.fillAmount <= 0)
            {
                ability1.fillAmount = 0;
                isCoolDown = false;
                check1 = false;

            }

            //   skill1.GetComponent<Image>().enabled = false;
            //   touch1.GetComponent<Image>().enabled = false;


        }
        
     
      
    }
    public void Ability2()
    {
        check2 = true;
        if (Input.touchCount > 0 && isCoolDown2 == false)
        {
            isCoolDown2 = true;
            ability2.fillAmount = 1;
            //check1 = true;
        }

        if (isCoolDown2)
        {
            ability2.fillAmount -= 1 / coolDown * Time.deltaTime;
            if (ability2.fillAmount <= 0)
            {
                ability2.fillAmount = 0;
                isCoolDown2 = false;
                check2 = false;
            }
            else
            {

            }
        }
    }
}
